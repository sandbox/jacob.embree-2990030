<?php
/**
 * The submission resource definition.
 *
 * @return array
 */
function _submission_resource_definition() {
  return array(
    'submission' => array(
      'operations' => array(
        'retrieve' => array(
          'callback' => 'webform_service_get_submission_by_uuid',
          'args' => array(
            array(
              'name' => 'uuid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'string',
              'description' => 'The uuid of the submission.',
            ),
          ),
          'access callback' => 'webform_service_submission_access',
          'access arguments' => array('view'),
          'access arguments append' => TRUE,
        ),
        'create' => array(
          'file' => array('type' => 'inc', 'module' => 'webform_service', 'name' => 'resources/submission_resource'),
          'callback' => 'webform_service_submission_create',
          'args' => array(
            array(
              'name' => 'uuid',
              'optional' => FALSE,
              'source' => array('data' => 'webform'),
              'type' => 'string',
              'description' => 'The uuid of the webform.',
            ),
            array(
              'name' => 'submission',
              'optional' => FALSE,
              'source' => array('data' => 'submission'),
              'description' => 'The submission data for this webform.',
              'type' => 'array',
              'default value' => array(),
            ),
           ),// args
          'access callback' => 'webform_service_submission_access',
          'access arguments' => array('create'),
          'access arguments append' => TRUE,
        ),// create
        'update' => array(
          'file' => array('type' => 'inc', 'module' => 'webform_service', 'name' => 'resources/submission_resource'),
          'callback' => 'webform_service_submission_update',
          'args' => array(
            array(
              'name' => 'uuid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'string',
              'description' => 'The uuid of the submission.',
            ),
            array(
              'name' => 'submission',
              'optional' => FALSE,
              'source' => 'data',
              'description' => 'The submission data to update',
              'type' => 'array',
            ),
          ),
          'access callback' => 'webform_service_submission_access',
          'access arguments' => array('edit'),
          'access arguments append' => TRUE,
        ),
        'delete' => array(
          'file' => array('type' => 'inc', 'module' => 'webform_service', 'name' => 'resources/submission_resource'),
          'callback' => 'webform_service_submission_delete',
          'args' => array(
            array(
              'name' => 'uuid',
              'optional' => FALSE,
              'source' => array('path' => 0),
              'type' => 'string',
              'description' => 'The uuid of the submission.',
            ),
          ),
          'access callback' => 'webform_service_submission_access',
          'access arguments' => array('delete'),
          'access arguments append' => TRUE,
        ),
      ),
    ),
  );
}

/**
 * Generates form_state values from a webform and a submission object
 *
 * @param object $webform
 *   The webform object.
 * @param array $submission
 *   The submission array with the user input.
 *
 * @return array
 *   An associative array with submitted data in the format needed by Drupal for
 *   the proccess of the form.
 */
function webform_service_submission_generate_values($webform, $submission) {
  $values = array();

  if ((!empty($webform)) && (!empty($webform->webform)) && (!empty($submission))) {
    $values['form_id'] = 'webform_client_form_' . $webform->nid;
    $values['op'] = isset($submission['op']) ? $submission['op'] : $webform->webform['submit_text'];
    $values['details'] = array(
      'nid' => $webform->nid,
      'sid' => !empty($submission['submission']['sid']) ? $submission['submission']['sid'] : NULL,
      'uid' => !empty($submission['uid']) ? $submission['uid'] : $GLOBALS['user']->uid,
      'page_num' => 1,
      'page_count' => 1,
      'finished' => 0,
    );
    $values['submitted'] = isset($submission['submitted']) ? $submission['submitted'] : array();
  }

  return $values;
}

/**
 * Creates a new submission within a webform.
 *
 * Note that this function uses drupal_execute() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $media
 *   Array representing the attributes a media edit form would submit.
 * @return
 *   An associative array contained the new node's nid and, if applicable,
 *   the fully qualified URI to this resource.
 *
 * @see drupal_execute()
 */
function webform_service_submission_create($uuid, $submission) {
  // Get the webform and submission entities.
  $webform = webform_service_resource_load($uuid);

  global $user;

  // Setup form_state
  $form_state = array();
  $form_state['values'] = webform_service_submission_generate_values($webform, $submission);
  $form_state['programmed_bypass_access_check'] = FALSE;
  $form_state['no_cache'] = TRUE;

  $stub_form_state = array(
    'no_cache' => TRUE,
    'build_info' => array(
      'args' => array($webform),
    ),
  );
  $stub_form = drupal_build_form('webform_client_form_' . $webform->nid, $stub_form_state);
  // Contributed modules may check the triggering element in the form state.
  // The triggering element is usually set from the browser's POST request, so
  // we'll automatically set it as the submit action from here.
  $form_state['triggering_element'] = $stub_form['actions']['submit'];

  drupal_form_submit('webform_client_form_' . $webform->nid, $form_state, $webform);

  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }

  module_load_include('inc', 'webform', 'includes/webform.submissions');
  $sid = $form_state['values']['details']['sid'];
  return webform_service_get_submission($webform, webform_get_submission($webform->nid, $sid, TRUE));
}

/**
 * Updates a webform submission based on submitted values.
 *
 * Note that this function uses drupal_execute() to create new nodes,
 * which may require very specific formatting. The full implications of this
 * are beyond the scope of this comment block. The Googles are your friend.
 *
 * @param $uuid
 *   UUID of the webform we're editing.
 * @param $sid
 *   Submission ID of the submission we are editing.
 * @param $submission
 *   Array representing the submission.
 *
 * @return
 *   The submission sid.
 *
 * @see drupal_execute()
 */
function webform_service_submission_update($uuid, $submission) {
  // Get the webform and submission entities.
  $webform = webform_submission_uuid_webform($uuid);
  $current_submission = webform_submission_uuid_submission($uuid);

  // If there is a current submission id, we must be updating a submission, so
  // add it to the submission.
  if (!isset($submission['submission']['sid']) && isset($current_submission->sid)) {
    $submission['submission']['sid'] = $current_submission->sid;
  }

  global $user;

  // Setup form_state
  $form_state = array();
  $form_state['values'] = webform_service_submission_generate_values($webform, $submission);
  $form_state['programmed_bypass_access_check'] = FALSE;
  $form_state['no_cache'] = TRUE;

  $stub_form_state = array(
    'no_cache' => TRUE,
    'build_info' => array(
      'args' => array($webform, $current_submission),
    ),
  );
  $stub_form = drupal_build_form('webform_client_form_' . $webform->nid, $stub_form_state, $webform);
  // Contributed modules may check the triggering element in the form state.
  // The triggering element is usually set from the browser's POST request, so
  // we'll automatically set it as the submit action from here.
  $form_state['triggering_element'] = $stub_form['actions']['submit'];

  drupal_form_submit('webform_client_form_' . $webform->nid, $form_state, $webform, (object) $current_submission);

  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }

  $return = array(
    'sid' => $form_state['values']['details']['sid'],
    'uuid' => $current_submission->uuid,
  );
  if ($uri = services_resource_uri(array('submission', $uuid))) {
    $return['uri'] = $uri;
  }
  return $return;
}

/**
 * Delete a submission within a webform.
 *
 * @param $nid
 *   Node ID of the node we're deleting.
 * @return
 *   The node's nid.
 */
function webform_service_submission_delete($uuid) {
  // Get the webform entity.
  $webform = webform_submission_uuid_webform($uuid);
  $submission = webform_submission_uuid_submission($uuid);

  // If the entity exists.
  if ($webform && $submission) {
    module_load_include('inc', 'webform', 'includes/webform.submissions');
    webform_submission_delete($webform, webform_service_parse_submission($webform, $submission));
    return TRUE;
  }
  else {
    return FALSE;
  }
}
